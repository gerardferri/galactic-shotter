﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class escenas : MonoBehaviour
{
    public void PulsaPlay(){
        Debug.LogError("He pulsado Play");
        SceneManager.LoadScene("Gameplay");
    }
    public void PulsaExit(){
        Application.Quit();
    }
    public void PulsaCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void PulsaBack()
    {
        SceneManager.LoadScene("Menu");
    }
}
