﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerBehaviour : MonoBehaviour
{
 
    public float speed;
    private Vector2 axis;
    public Vector2 limits;

    

    public Image health;
    public Image health2;
    public Image health3;
    float hp, maxHp = 100f;


    public GameObject shield;

    private float shootTime=0;
 
    public Weapon weapon;
    
    public Propeller prop;
   
    [SerializeField] GameObject graphics;
    [SerializeField] Collider2D collidernave;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;
 
    public int lives = 3;
    private bool iamDead = false;
    private GameObject healthbar;
    private float amount;

    // Update is called once per frame
    void Start()
    {
        
            hp = maxHp;
        
        healthbar = GameObject.Find("Healthbar");
    }
    void Update () {
        if(iamDead){
            return;
        }
 
        shootTime += Time.deltaTime;
 
        transform.Translate (axis * speed * Time.deltaTime);
 
        if (transform.position.x > limits.x) {
            transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
        }else if (transform.position.x < -limits.x) {
            transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
        }
 
        if (transform.position.y > limits.y) {
            transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
        }else if (transform.position.y < -limits.y) {
            transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
        }
 
        if(axis.x>0){
            prop.BlueFire();
        }else if(axis.x<0){
            prop.RedFire();
        }else{
            prop.Stop();
        }
    }
 
    public void ActualizaDatosInput(Vector2 currentAxis){
        axis = currentAxis;
    }
 
    public void SetAxis(float x, float y){
        axis = new Vector2(x,y);
    }
 
    public void SetAxis(Vector2 currentAxis){
        axis = currentAxis;
    }
 
    public void Shoot(){
        if(shootTime>weapon.GetCadencia()){
            shootTime = 0f;
            weapon.Shoot();
        }

    }
   


    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    public void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Meteor") {
            StartCoroutine(DestroyShip());
        }
        if (other.tag == "Misil")
        {
            StartCoroutine(DestroyShip());
        }
        if (other.tag == "Enemy")
        {
            StartCoroutine(DestroyShip());
        }
        if (other.tag== "Medikit")
        {
            lives++;
            health.gameObject.SetActive(true);
            health2.gameObject.SetActive(true);
            health3.gameObject.SetActive(true);
        }
        if (other.tag == "Shield")
        {
            StartCoroutine(Useshield());
            
        }
    }
    IEnumerator Useshield()
    {
        //Desactivo el grafico
        shield.gameObject.SetActive(true);

        //Elimino el BoxCollider2D
        collidernave.enabled = false;
        yield return new WaitForSeconds(5.0f);
        shield.gameObject.SetActive(false);
        collidernave.enabled = true;
    }


    IEnumerator DestroyShip(){
        //Indico que estoy muerto
        iamDead=true;
 
        //Me quito una vida
        lives--;
        if (lives == 2)
        {

            health.gameObject.SetActive(false);
        }
        if (lives == 1)
        {

            health2.gameObject.SetActive(false);
        }
        if (lives == 0)
        {

            health3.gameObject.SetActive(false);
        }
        //Desactivo el grafico
        graphics.SetActive(false);
 
        //Elimino el BoxCollider2D
        collidernave.enabled = false;
 
        //Lanzo la partícula
        ps.Play();
 
        //Lanzo sonido de explosion
        audioSource.Play();
 
        //Desactivo el propeller
        prop.gameObject.SetActive(false);
 
        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);
       

        //Miro si tengo mas vidas
        if(lives>0){

            //Vuelvo a activar el jugador
            iamDead = false;

            
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.3f);
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.3f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.3f);
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.3f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.3f);
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.3f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.3f);
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.3f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.3f);
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.3f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.3f);
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.3f);
            graphics.SetActive(true);
            collidernave.enabled = true;
            //Activo el propeller
            prop.gameObject.SetActive(true);
        }
        if (lives == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
    
}